package Net::VoIP_Innovations;

use warnings;
use strict;
use Data::Dumper;
use SOAP::Lite;
#SOAP::Lite->import(+trace=>'debug');

=head1 NAME

Net::VoIP_Innovations - Interface to VoIP Innovations API

=cut

our $VERSION = '3.00_01';
our $URI     = 'http://dev.voipinnovations.com/VOIP/Services/APIService.asmx';
our $NS      = 'http://tempuri.org'; #nice one

our $AUTOLOAD;
our $errstr = '';

=head1 SYNOPSIS

    use Net::VoIP_Innovations 3;

    my $voip_innovations = Net::VoIP_Innovations->new(
        'login'    => 'tofu',
        'password' => 'beast', #secret
    );


    ##
    # DID functions
    ##

    #auditDIDs
    #queryDID
    #reserveDID
    #assignDID
    #configDID
    #releaseDID


    ###
    # 911 Functions
    ###

    #insert911
    my $response = $voip_innovations->insert911(
      'did'        => '4155551212',
      'address1'   => '1234 Test Lane',
      'address2'   => '',
      'city'       => 'Testington',
      'state'      => 'CA',
      'zip'        => '95454',
      'plusFour'   => '',
      'callerName' => 'Joe Caller',
    );
    if ( $response->{'responseCode'} != 100 ) {
      die $response->{'responseMessage'};
    }

    #update911
    my $response = $voip_innovations->update911(
      'did'        => '4155551212',
      'address1'   => '1234 Test Lane',
      'address2'   => '',
      'city'       => 'Testington',
      'state'      => 'CA',
      'zip'        => '95454',
      'plusFour'   => '',
      'callerName' => 'Joe Caller',
    );
    if ( $response->{'responseCode'} != 100 ) {
      die $response->{'responseMessage'};
    }

    #remove911
    my $response = $voip_innovations->remove911(
      'did'        => '4155551212',
    );
    if ( $response->{'responseCode'} != 100 ) {
      die $response->{'responseMessage'};
    }


    ###
    # Locator Functions
    ###

    ...

=head1 METHODS

=head2 new HASHREF | OPTION, VALUE ...

Creates a new Net::VoIP_Innovations object.  Options may be passed
as a hash reference or a flat list of names and values.

=over 4

=item login (required)

=item password (secret) (required)

=back

=cut

#  If there is an error,
#returns false and sets an error string which may be queried with the I<errstr>
#class method.

sub new {
  my $proto = shift;
  my $class = ref($proto) || $proto;
  my $self = ref($_[0]) ? shift : { @_ };
  bless($self, $class);
}

=head2 errstr

=cut

sub errstr {
  my $class = shift;

  return $errstr
    unless ref($class) && $class->isa('Net::VoIP_Innovations');

  my $self = $class;
  $self->{errstr};
}

sub DESTROY { }; # no-op

sub AUTOLOAD {
  my $self = shift;
  my $opts = ref($_[0]) ? shift : { @_ };

  $AUTOLOAD =~ /(^|::)(\w+)$/ or die "unparsable AUTOLOAD: $AUTOLOAD";
  my $function = $2;

  $opts->{'login'}  ||= $self->{'login'};
  $opts->{'secret'} ||= $self->{'password'};
  my @soap_opts = map { SOAP::Data->name($_)->value( $opts->{$_} ) }
                    keys %$opts;

  SOAP::Lite
    ->proxy($URI)
    #->uri($NS)
    ->default_ns($NS)
    ->on_action( sub { join '/', @_ } )
    ->$function( @soap_opts )
    ->result();

}

=head1 AUTHOR

Ivan Kohler, C<< <ivan-net-voipinnovations at freeside.biz> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-net-voip_innovations at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=Net-VoIP_Innovations>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.


=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc Net::VoIP_Innovations


You can also look for information at:

=over 4

=item * RT: CPAN's request tracker

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=Net-VoIP_Innovations>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/Net-VoIP_Innovations>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/Net-VoIP_Innovations>

=item * Search CPAN

L<http://search.cpan.org/dist/Net-VoIP_Innovations>

=back


=head1 COPYRIGHT & LICENSE

Copyright 2008-2014 Freeside Internet Services, Inc. (http://freeside.biz/)

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.


=head1 ADVERTISEMENT

Need a complete, open-source back-office and customer self-service solution?
The Freeside software includes support for VoIP Innovations integration, CDR
rating, invoicing, credit card and electronic check processing, integrated
trouble ticketing, and customer signup and self-service web interfaces.

L<http://freeside.biz/freeside/>

=cut

1;
