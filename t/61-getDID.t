#!/usr/bin/perl -w

#BEGIN {
#  $Net::HTTPS::Any::skip_NetSSLeay = 1;
#  $Net::HTTPS::Any::skip_NetSSLeay = 1;
#}

use Test::More;

require 't/lib/test_account.pl';
require Net::VoIP_Innovations;

my($login, $password) = test_account_or_skip();
plan tests => 1;

my $debug = $ENV{TEST_VERBOSE};

my $gp = Net::VoIP_Innovations->new( 'login'    => $login,
                                     'password' => $password,
                                     #'debug'    => $debug,
                                   );
my $return = $gp->getDID();

use Data::Dumper;

#test some things about the return...
ok( $return->{type} ne 'Error', 'getDID returned no error' );
diag( Dumper($return) ) if $return->{type} eq 'Error';

