#!perl -T

use Test::More tests => 1;

BEGIN {
	use_ok( 'Net::VoIP_Innovations' );
}

diag( "Testing Net::VoIP_Innovations $Net::VoIP_Innovations::VERSION, Perl $], $^X" );
